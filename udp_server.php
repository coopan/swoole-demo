<?php

$serv = new Swoole\Server('127.0.0.1', 9502, SWOOLE_PROCESS, SWOOLE_SOCK_UDP);

// 监听数据接收事件
$serv->on('Packet', function ($serv, $data, $clientInfo) {

    // 使用 sendto() 方法向客户端发送数据
    $serv->sendto($clientInfo['address'], $clientInfo['port'], "Server:" . $data);

    var_dump($clientInfo);
});

$serv->start();

/**
 * UDP服务器与TCP不同，UDP没有连接的概念。启动Server后，客户端无需 Connect，直接可以向 Server 监听的 9502 端口发送数据包。对应的事件为 onPacket
 *
 * $clientInfo 的信息大概如下：
 * array(4) {
    ["server_socket"]=>
        int(3)
    ["server_port"]=>
        int(9502)
    ["address"]=>
        string(9) "127.0.0.1"
    ["port"]=>
        int(49385)
    }
 */


/**
 * UDP 服务器可以用 netcat -u 来连接测试
 * netcat -u 127.0.0.1 9502
 * 输入 Hello
 * Server: Hello
 */